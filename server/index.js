const csvToJson = require("csvtojson");
const fs = require("fs");
const path = require("path");
const { matchesPerYear,
     matchesWonPerTeamPerYear, 
     extraRunsPerTeam16, 
     top10EconomicalBowlers15, 
     tossAndMatchWonByTeam,
     highestPlayerOfTheMatchPerSeason,
     strikeRateOfABatsmenEachSeason,
     highestDismissalByOnePlayerOfAnother} = require(path.join(__dirname, "../server/ipl"));
const deliveriesJSON = require(path.join(__dirname, "../data/deliveries.json"));
const matchesJSON = require(path.join(__dirname, "../data/matches.json"));

const csvFilePath1 = path.join(__dirname, '../data/deliveries.csv');
const csvFilePath2 = path.join(__dirname, '../data/matches.csv');

csvToJson()
    .fromFile(csvFilePath1)
    .then((json => {
        fs.writeFile(path.join(__dirname, '../data/deliveries.json'), JSON.stringify(json), "utf-8", (err) => {
            if (err) return console.log(err);
        });
    }))
    .catch(err => console.log(err));


csvToJson()
    .fromFile(csvFilePath2)
    .then((json) => {
        fs.writeFile(path.join(__dirname, '../data/matches.json'), JSON.stringify(json), "utf-8", (err) => {
            if (err) return console.log(err);
        });
    })
    .catch(err => console.log(err));


fs.writeFile(path.join(__dirname, '../public/output/matchesPerYear.json'), JSON.stringify(matchesPerYear(matchesJSON)), "utf-8", (err) => {
    if (err) return console.log(err);
});

fs.writeFile(path.join(__dirname, '../public/output/matchesWonPerTeamPerYear.json'), JSON.stringify(matchesWonPerTeamPerYear(matchesJSON)), "utf-8", (err) => {
    if (err) return console.log(err);
});

fs.writeFile(path.join(__dirname, '../public/output/extraRunsPerTeam16.json'), JSON.stringify(extraRunsPerTeam16(deliveriesJSON)), "utf-8", (err) => {
    if (err) return console.log(err);
});

fs.writeFile(path.join(__dirname, '../public/output/top10EconomicalBowlers15.json'), JSON.stringify(top10EconomicalBowlers15(matchesJSON, deliveriesJSON)), "utf-8", (err) => {
    if (err) return console.log(err);
});

fs.writeFile(path.join(__dirname, '../public/output/tossAndMatchWonByTeam.json'), JSON.stringify(tossAndMatchWonByTeam(matchesJSON)), "utf-8", (err) => {
    if (err) return console.log(err);
});

fs.writeFile(path.join(__dirname, '../public/output/highestPlayerOfTheMatchPerSeason.json'), JSON.stringify(highestPlayerOfTheMatchPerSeason(matchesJSON)), "utf-8", (err) => {
    if (err) return console.log(err);
});

fs.writeFile(path.join(__dirname, '../public/output/strikeRateOfABatsmenEachSeason.json'), JSON.stringify(strikeRateOfABatsmenEachSeason("S Dhawan", matchesJSON, deliveriesJSON)), "utf-8", (err) => {
    if (err) return console.log(err);
});

fs.writeFile(path.join(__dirname, '../public/output/highestDismissalByOnePlayerOfAnother.json'), JSON.stringify(highestDismissalByOnePlayerOfAnother(deliveriesJSON)), "utf-8", (err) => {
    if (err) return console.log(err);
});
