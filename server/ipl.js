function matchesPerYear(matchesJSON) {
    let obj = {}

    for (let match of matchesJSON) {
        if (typeof obj[match.season] === "undefined") {
            obj[match.season] = matchesJSON.filter(curr => curr.season == match.season).length;
        }
    }

    return obj;
}

function seasonWinsList(season, JSON) {
    let result = {}

    for (let match of JSON) {
        if (typeof result[match.winner] === "undefined") {
            result[match.winner] = JSON.filter(curr => curr.winner == match.winner).length;
        }
    }

    return result;
}

function matchesWonPerTeamPerYear(matchesJSON) {
    let winners = {}

    for (let match of matchesJSON) {
        if (typeof winners[match.season] === "undefined") {
            winners[match.season] = seasonWinsList(match.season, matchesJSON.filter(curr => match.season === curr.season));
        }
    }

    return winners;
}

function extraRunsPerTeam16(deliveriesJSON) {
    let extraRuns = {}

    deliveriesJSON.filter(data => data.match_id >= 577).forEach(match => {
        if (typeof extraRuns[match.bowling_team] === "undefined") {
            extraRuns[match.bowling_team] = deliveriesJSON.filter(data => data.match_id >= 577)
                .filter(data => data.bowling_team === match.bowling_team)
                .reduce((extra, curr) => extra + parseInt(curr.extra_runs), 0);
        }
    });

    return extraRuns;
}

function top10EconomicalBowlers15(matchesJSON, deliveriesJSON) {
    let economicalBowlers = {}
    let matches16 = deliveriesJSON.filter(data => data.match_id >= 518 && data.match_id <= 576)

    matches16.forEach(match => {
        if (typeof economicalBowlers[match.bowler] == "undefined") {
            let overs = [];
            let array = matches16.filter(data => data.bowler === match.bowler)

            array.filter(balls => {
                if (!overs.includes(`${balls.over} ${balls.match_id}`)) {
                    overs.push(`${balls.over} ${balls.match_id}`)
                }
            })

            const runs = array.reduce((total, ball) => total + parseInt(ball.total_runs), 0);

            economicalBowlers[match.bowler] = runs / overs.length;
        }
    });

    const result = Object.entries(economicalBowlers).sort(([, a], [, b]) => a - b)
        .filter((entry, index) => index < 10).map(element => {
            let obj = {}
            obj[element[0]] = element[1]

            return obj;
        })

    return result;
}

function tossAndMatchWonByTeam(matchesJSON) {
    const result = {}
    const teams = []
    matchesJSON.forEach(match => {
        if (!teams.includes(match.toss_winner)) {
            result[match.toss_winner] = matchesJSON.filter(curr => match.toss_winner === curr.toss_winner)
                .reduce((count, curr) => curr.toss_winner === curr.winner ? count += 1 : count, 0)

            teams.push(match.toss_winner);
        }
    })

    return result;
}

function highestPlayerOfTheMatchPerSeason(matchesJSON) {
    const myObj = {}

    matchesJSON.forEach(match => {
        if (typeof myObj[match.season] === "undefined") {
            const matchesOfCurrYear = matchesJSON.filter(curr => curr.season === match.season)
            const obj = {}

            matchesOfCurrYear.forEach(match => {
                if (typeof obj[match.player_of_match] == "undefined") {
                    obj[match.player_of_match] = matchesOfCurrYear.reduce((count, curr) => curr.player_of_match == match.player_of_match ? count += 1 : count, 0);
                }
            })

            let max = ""
            let maxValue = 0

            for (const [key, value] of Object.entries(obj)) {
                if (max === "") {
                    max = key;
                    maxValue = value;
                } else {
                    if (obj[max] < value) {
                        max = key;
                        maxValue = value;
                    }
                }
            }

            const resultObj = {}

            resultObj[max] = maxValue;

            myObj[match.season] = resultObj;
        }
    })

    return myObj;

}

function strikeRateOfABatsmenEachSeason(batsmanName, matchesJSON, deliveriesJSON) {
    const seasons = {}
    const result = {}

    matchesJSON.forEach(element => {
        seasons[element.season] = matchesJSON.filter(match => match.season == element.season ? match.id : 0)
            .map(el => el.id)
    })

    for (const season of Object.keys(seasons)) {
        const eachBatsman = {}
        const strikeRate = {}
        const matchesInSeason = deliveriesJSON
            .filter(el => {
                if (parseInt(el.match_id) >= seasons[season][0] && parseInt(el.match_id) <= seasons[season][seasons[season].length - 1]) {
                    return true;
                }
            })

        matchesInSeason.forEach(el => {
            eachBatsman[el.batsman] = { total_runs: 0, count: 0 }
        })

        matchesInSeason.forEach(el => {
            eachBatsman[el.batsman].total_runs += parseInt(el.batsman_runs);
            eachBatsman[el.batsman].count += 1;

            strikeRate[el.batsman] = (eachBatsman[el.batsman].total_runs / eachBatsman[el.batsman].count * 100).toFixed(2);
        })

        const resultObj = {}

        for(const obj in strikeRate){
            if(obj===batsmanName){ 
                resultObj[obj] = strikeRate[obj];
            }
        }

        result[season] = resultObj;
    }

    return result;
}

function highestDismissalByOnePlayerOfAnother(deliveriesJSON){
    let str = '';
    let highest = 0;
    const dismissal = {}
    const result = {}
    

    deliveriesJSON.forEach(el => {
        if(el.player_dismissed.length > 2){
            dismissal[`${el.bowler} - ${el.player_dismissed}`]= 0;
        }
    })

    deliveriesJSON.forEach(el => {
        if(el.player_dismissed.length > 2){
            dismissal[`${el.bowler} - ${el.player_dismissed}`]+=1;
        }

        if(dismissal[`${el.bowler} - ${el.player_dismissed}`] > highest){
            highest = dismissal[`${el.bowler} - ${el.player_dismissed}`];
            str = `${el.bowler} - ${el.player_dismissed}`;
        }
    })

    result[str] = highest;
    
    return result;
}

module.exports.matchesPerYear = matchesPerYear;
module.exports.matchesWonPerTeamPerYear = matchesWonPerTeamPerYear;
module.exports.extraRunsPerTeam16 = extraRunsPerTeam16;
module.exports.top10EconomicalBowlers15 = top10EconomicalBowlers15;
module.exports.tossAndMatchWonByTeam = tossAndMatchWonByTeam;
module.exports.highestPlayerOfTheMatchPerSeason = highestPlayerOfTheMatchPerSeason;
module.exports.strikeRateOfABatsmenEachSeason = strikeRateOfABatsmenEachSeason;
module.exports.highestDismissalByOnePlayerOfAnother = highestDismissalByOnePlayerOfAnother;