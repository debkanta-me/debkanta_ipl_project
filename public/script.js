function matchesPerYearPerTeam() {
    Highcharts.setOptions({
        colors: ['#1c275c']
    });

    fetch("./output/matchesPerYear.json")
        .then(data => data.json()).then(data => {
            Highcharts.chart('container1', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Number of matches played per year for all the years in IPL'
                },
                xAxis: {
                    type: 'category',
                    labels: {
                        rotation: 0,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'No. of Matches'
                    }
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    pointFormat: 'Extra runs conceded: <b>{point.y}</b>',
                    backgroundColor: '#90ee90'
                },
                series: [{
                    name: 'Population',
                    data: Object.entries(data),
                    dataLabels: {
                        enabled: true,
                        rotation: -60,
                        color: '#FFFFFF',
                        align: 'right',
                        format: '{point.y}', // one decimal
                        y: 30, // 10 pixels down from the top
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                }]
            });
        })
}

function extraRunsPerTeam16() {
    fetch("./output/extraRunsPerTeam16.json")
        .then(data => data.json()).then(data => {
            Highcharts.chart('container3', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Extra Runs Per Team 2016'
                },
                xAxis: {
                    type: 'category',
                    labels: {
                        rotation: 0,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Extra Runs'
                    }
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    pointFormat: 'Extra runs conceded: <b>{point.y}</b>',
                    backgroundColor: '#90ee90'
                },
                series: [{
                    name: 'Population',
                    data: Object.entries(data),
                    dataLabels: {
                        enabled: true,
                        rotation: -60,
                        color: '#FFFFFF',
                        align: 'right',
                        format: '{point.y}', // one decimal
                        y: 30, // 10 pixels down from the top
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                }]
            });
        })
}

function matchesWonPerTeamPerYear() {
    fetch("./output/matchesWonPerTeamPerYear.json")
        .then(data => data.json())
        .then(data => {
            const arr = []

            for (const key of Object.keys(data)) {
                arr.push({
                    "name": key,
                    "y": Object.values(data[key]).reduce((sum, element) => sum + element, 0),
                    "drilldown": key
                })
            }

            const drillArray = []

            for (const key of Object.keys(data)) {
                drillArray.push({
                    'name': key,
                    'id': key,
                    'data': Object.keys(data[key]).map(element => {
                        return [element, data[key][element]]
                    })
                })
            }
            Highcharts.setOptions({
                colors: ['#058DC7', '#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4', "black"]
            });

            Highcharts.chart('container2', {
                chart: {
                    type: 'pie'
                },
                title: {
                    text: 'Number of matches won per team per year in IPL'
                },
                subtitle: {
                    text: 'Click the columns to view versions.'
                },
                accessibility: {
                    announceNewData: {
                        enabled: true
                    }
                },
                xAxis: {
                    type: 'category'
                },
                yAxis: {
                    title: {
                        text: 'Number of matches per season'
                    }

                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true,
                            format: '{point.y}'
                        }
                    }
                },

                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{black}"\>{point.name}</span>: <b>{point.y}</b> of total<br/>'
                },

                series: [
                    {
                        name: "Matches",
                        colorByPoint: true,
                        data: arr,
                    }
                ],
                drilldown: {
                    series: drillArray
                }
            });
        })
}

function top10EconomicalBowlers15() {
    fetch("./output/top10EconomicalBowlers15.json")
        .then(data => data.json())
        .then(data => {
            const seriesData = data.map(element => {
                return { "name": Object.keys(element)[0], "y": Object.values(element)[0] }
            })

            Highcharts.chart('container4', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'Top 10 economical bowlers in the year 2015'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.y}</b>'
                },
                accessibility: {
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false
                        },
                        showInLegend: true
                    }
                },
                series: [{
                    name: 'Avg Run (per over)',
                    colorByPoint: true,
                    data: seriesData
                }]
            });
        })
}

function tossAndMatchWonByTeam() {
    fetch("./output/tossAndMatchWonByTeam.json")
        .then(data => data.json())
        .then(data => {
            Highcharts.chart('container5', {
                chart: {
                    type: 'line'
                },
                title: {
                    text: 'Number of times each team won the toss and also won the match in IPL',
                },
                xAxis: {
                    type: 'category',
                    labels: {
                        rotation: 0,
                        style: {
                            fontSize: '10px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'No. of Matches'
                    }
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    pointFormat: 'No. of times won the match and toss: <b>{point.y}</b>',
                    backgroundColor: '#90ee90'
                },
                series: [{
                    name: 'Population',
                    data: Object.entries(data),
                    dataLabels: {
                        enabled: true,
                        rotation: -60,
                        color: '#FFFFFF',
                        align: 'right',
                        format: '{point.y}', // one decimal
                        y: 30, // 10 pixels down from the top
                        style: {
                            fontSize: '15px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                }]
            });
        })
}

function highestPlayerOfTheMatchPerSeason() {
    fetch("./output/highestPlayerOfTheMatchPerSeason.json")
        .then(data => data.json())
        .then(data => {
            const arr = []

            for (const key of Object.keys(data)) {
                arr.push({
                    "name": key,
                    "y": Object.values(data[key])[0],
                    "drilldown": key
                })
            }

            const drillArray = []

            for (const key of Object.keys(data)) {
                drillArray.push({
                    'name': Object.keys(data[key])[0],
                    'id': key,
                    'data': [Object.keys(data[key])[0], Object.values(data[key])[0]]
                })
            }

            // Create the chart
            Highcharts.chart('container6', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'A player who has won the highest number of Player of the Match awards for each season'
                },
                subtitle: {
                    text: 'Click the columns to view the name of the Batsman.'
                },
                accessibility: {
                    announceNewData: {
                        enabled: true
                    }
                },
                xAxis: {
                    type: 'category'
                },
                yAxis: {
                    title: {
                        text: 'No. of Player of the Match'
                    }

                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true,
                            format: '{point.y}'
                        }
                    }
                },

                tooltip: {
                    headerFormat: '<span style="font-size:11px"></span><br>',
                    pointFormat: '<span style="color:{black}">{series.name}</span>: <b>{point.y}</b> of total<br/>'
                },

                series: [
                    {
                        name: "Total no. of Player of the match",
                        colorByPoint: true,
                        data: arr
                    }
                ],
                drilldown: {
                    series: drillArray
                }
            });
        })
}

function strikeRateOfABatsmenEachSeason(){
    Highcharts.setOptions({
        colors: ['#1c275c']
    });
    
    fetch("./output/strikeRateOfABatsmenEachSeason.json")
    .then(data => data.json())
    .then(data => {
        let playerName = "";
        const obj = {}
        for(const element in data){
            if(playerName == ""){
                playerName = Object.keys(data[element])[0];
            }
            obj[element]= parseInt(Object.values(data[element])[0]);
        }
        //
        Highcharts.chart('container7', {
            chart: {
                type: 'column'
            },
            title: {
                text: `Strike rate of <b>${playerName}</b> for each season`
            },
            xAxis: {
                type: 'category',
                labels: {
                    rotation: 0,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Avg. run per match'
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                pointFormat: 'Strike rate: <b>{point.y:.2f}</b>',
                backgroundColor: '#90ee90'
            },
            series: [{
                name: 'Population',
                data: Object.entries(obj),
                dataLabels: {
                    enabled: true,
                    rotation: -60,
                    color: '#FFFFFF',
                    align: 'right',
                    format: '{point.y}', // one decimal
                    y: 30, // 10 pixels down from the top
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            }]
        });
        //
    })
}



matchesPerYearPerTeam();
extraRunsPerTeam16();
matchesWonPerTeamPerYear();
top10EconomicalBowlers15();
tossAndMatchWonByTeam();
highestPlayerOfTheMatchPerSeason();
strikeRateOfABatsmenEachSeason();